# Piggyback Parameters Manual

###### \[in package PIGGYBACK-PARAMETERS\]
[![pipeline status](https://gitlab.com/ediethelm/piggyback-parameters/badges/master/pipeline.svg)](https://gitlab.com/ediethelm/piggyback-parameters/commits/master)
[![Quicklisp](http://quickdocs.org/badge/piggyback-parameters.svg)](http://quickdocs.org/piggyback-parameters/)
[![coverage report](https://gitlab.com/ediethelm/piggyback-parameters/badges/master/coverage.svg?job=test-coverage)](https://gitlab.com/ediethelm/piggyback-parameters/-/jobs/artifacts/master/browse?job=test-coverage)

## Description

This is a parameter system that supports local file and database parameter storage.
The library *trivial-pooled-database* is used to handle the connection and **must** be initialized prior to reading any parameters from the database. See section **Working Example**.  
Loading this parameters from the local configuration file is possible using the *:local-only* strategy.  

The database table for storing parameters must be created with following SQL statement

```sql
CREATE TABLE `_OPTIONS_` (
  `parameter` varchar(64) NOT NULL,
  `value` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```


## Installing piggyback-parameters

This project is available in the latest [QuickLisp](https://www.quicklisp.org/beta/ "QuickLisp") distribution, so installing it is reduced to calling:

```lisp
(ql:quickload :piggyback-parameters)
```


## Working Example

First load local configuration file.

```lisp
(piggyback-parameters:load-config (uiop/common-lisp:merge-pathnames #P"config" (asdf:system-source-directory :some_project)))
```

Now it is possible to use the loaded parameters to initialize the DB connection.

```lisp
(let ((username (piggyback-parameters:get-value :username :file-only))
	(passwd (piggyback-parameters:get-value :passwd :file-only))
	(schema (piggyback-parameters:get-value :schema :file-only))
	(host (piggyback-parameters:get-value :host :file-only)))
  (log:info "Initializing DB Connections Pool (~a ~a ~a ~a)"  username passwd schema host)
  (trivial-pooled-database:initialize-connection-pool username passwd schema host))
```

Let's save some parameter in the DB...

```lisp
(setf (piggyback-parameters:get-value :enabled-plugins :database-only) '("plugin-a" "plugin-b" "plugin-c"))
```

... and at some later point read them back.

```lisp
(loop for plugin in (piggyback-parameters:get-value :enabled-plugins)
   do (log:info "Loading plugin '~a'." plugin))
```


## Exported Symbols

- [function] RESET

    Reset the state of the in-memory configuration.

- [function] SET-CONFIGURATION-FILE PATH

    Define the path of the configuration file to be used in combination with the :file-only and :file-first strategies.

- [function] GET-VALUE PARAMETER &OPTIONAL STRATEGY FORCE

    Read the value of paramter *PARAM*. The source of the value can be defined by *STRATEGY*, which accepts following values  
    **nil** Reuse the same strategy as in the previous get/set of this parameter;  
    **:file-first** Load the value first from the local configuration and, if it does not exist, from the database;  
    **:db-first** Load the value first from the database and, if it does not exist, from the local configuration;  
    **:try-append** Load the values both from database and from local configuration and try to append them (valid types are only *list* and *string*);  
    **:file-only** Load the value only from the local configuration file;  
    **:db-only** Load the value only from the database.

- [function] (SETF GET-VALUE) VALUE PARAMETER &OPTIONAL STRATEGY

    Save the parameter *PARAMETER* with value *VALUE* into storage. The storage destination is defined by the parameter *STRATEGY* with possible values being  
    **nil** Reuse the same strategy as in the previous get/set of this parameter;  
    **:local-only** Save the parameter/value pair only in the local configuration file;  
    **:db-only** Save the parameter/value pair only in the database.

- [function] CLEAR-PARAMETER PARAMETER

    Remove the parameter from memory (aka unload).

- [function] DELETE-PARAMETER PARAMETER FROM

## License Information

This library is released under the MIT License. Please refer to the [LICENSE](https://gitlab.com/ediethelm/piggyback-parameters/blob/master/LICENSE "License") to get the full licensing text.

## Contributing to this project

Please refer to the [CONTRIBUTING](https://gitlab.com/ediethelm/piggyback-parameters/blob/master/CONTRIBUTING "Contributing") document for more information.

