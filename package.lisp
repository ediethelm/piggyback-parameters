(uiop:define-package #:piggyback-parameters
  (:documentation "piggyback-parameters is a parameter system that supports local file and database parameter storage.")
  (:use #:common-lisp)
  (:export #:file-not-found-error
	   #:set-configuration-file
           #:get-value
	   #:clear-parameter
	   #:delete-parameter
	   #:reset))

