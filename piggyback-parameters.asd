;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(defsystem :piggyback-parameters
  :name "piggyback-parameters"
  :description "This is a configuration system that supports local file and database based parameter storage."
  :version "0.1.7"
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "MIT"
  :depends-on (:trivial-pooled-database
	       :trivial-hashtable-serialize
	       :trivial-json-codec)
  :in-order-to ((test-op (test-op :piggyback-parameters/test)))
  :components ((:file "package")
	       (:file "piggyback-parameters")))

(defsystem :piggyback-parameters/test
  :name "piggyback-parameters/test"
  :description "Unit Tests for the piggyback-parameters project."
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "MIT"
  :depends-on (:piggyback-parameters fiveam)
  :perform (test-op (o s) (uiop:symbol-call :fiveam  '#:run! :piggyback-parameters-tests))
  :components ((:file "test-piggyback-parameters")))

