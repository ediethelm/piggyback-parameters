;;;; Copyright (c) Eric Diethelm 2018 <ediethelm@yahoo.com>
;;;; This code is licensed under the MIT license (unless declared otherwise).

(in-package :piggyback-parameters)


(defvar *config-file* nil)
(defvar *parameters* nil)

(define-condition file-not-found-error (error)
   ((path :initarg :path :reader path)))

(defclass config-parameter ()
  ((key :reader get-key
	:initarg :key
	:initform #'(lambda () (error "The configuration parameter must have a key (the parameter name).")))
   (value :initarg :value
	  :initform nil)
   (read-strategy :reader strategy
		  :initarg :strategy
		  :initform :file-first
		  :type (member :memory-only :file-first :database-first :file-only :database-only))))

(defun load-file (path)
  (unless (uiop:probe-file* path)
    (error 'file-not-found-error :path path))

  (trivial-json-codec:deserialize-json
   (format nil "~{~a~}"
	   (with-open-file (stream path)
	     (loop for line = (read-line stream nil)
		while line
		collect line)))))

(defun save-file (path)
  (uiop:ensure-pathname path :ensure-directories-exist t)

  (with-open-file (stream path
			  :direction :output
			  :if-exists :supersede
			  :if-does-not-exist :create)
    (format stream "~a"
	    (trivial-json-codec:serialize-json (mapcar #'(lambda (x) (list (get-key x) (slot-value x 'value)))
						       (remove-if-not #'(lambda (x) (or (eq (strategy x) :file-only)
											(eq (strategy x) :file-first)))
								      *parameters*))))))

(defun try-merge (fval sval)
  (cond
    ((null fval)
     sval)
    ((null sval)
     fval)
    ((and fval
	  sval
	  (not (eq (class-of fval) (class-of sval))))
     (log:error "Cannot combine values of types '~a' and '~a'." (class-of fval) (class-of sval))
     (values nil nil))
    ((or (and (stringp fval) (stringp sval))
	 (and (listp fval) (listp sval)))
     (values (concatenate (class-of (or fval sval)) fval sval) t))
    (t (log:error "Cannot combine values of type '~a'. Only string and list are supported." (class-of (or fval sval)))
       (values nil nil))))

(defun find-parameter (key)
  (car (member key *parameters* :key #'get-key)))

(defgeneric load-value (parameter strategy)) ;; @TODO Use second value to indicate if param was even found (as in gethash)
(defgeneric save-value (parameter strategy value))

(defmethod load-value (parameter (strategy (eql :database-only)))
  (values (trivial-json-codec:deserialize-raw
	   (cadar
	    (trivial-pooled-database:select "_OPTIONS_"
					    "value"
					    (format nil "parameter=\":~a\"" parameter))))))

(defmethod load-value (parameter (strategy (eql :file-only)))
  (let ((val (cadar (member parameter (load-file *config-file*) :key #'car))))
    (values val)))

(defmethod load-value (parameter (strategy (eql :memory-only)))
  (values nil))

(defmethod load-value (parameter (strategy (eql :file-first)))
  (let ((file-val (load-value parameter :file-only))
	(db-val (load-value parameter :database-only)))
    (multiple-value-bind (val ok)
	(try-merge file-val db-val)
      (if ok
	  val
	  file-val))))

(defmethod load-value (parameter (strategy (eql :database-first)))
  (let ((db-val (load-value parameter :database-only))
	(file-val (load-value parameter :file-only)))
    (multiple-value-bind (val ok)
	(try-merge db-val file-val)
      (if ok
	  val
	  db-val))))

(defmethod save-value (parameter (strategy (eql :database-only)) value)
  ;; @TODO We still have to handle parameters not yet in the database!
  (trivial-pooled-database:update "_OPTIONS_" "value" (trivial-json-codec:serialize-json value) (format nil "parameter=\":~a\"" parameter))
  (values value))

(defmethod save-value (parameter (strategy (eql :file-only)) value)
  (save-file *config-file*)
  (values value))

(defmethod save-value (parameter (strategy (eql :memory-only)) value)
  (values value))

(defmethod save-value (parameter (strategy (eql :file-first)) value)
  (log:error "It is not possible to save a parameter that was potentially merged during load.")
  (cerror "Don't save and return." "It is not possible to save a parameter that was potentially merged during load.")
  (values value))

(defmethod save-value (parameter (strategy (eql :database-first)) value)
  (log:error "It is not possible to save a parameter that was potentially merged during load.")
  (cerror "Don't save and return." "It is not possible to save a parameter that was potentially merged during load.")
  (values value))


;;----------------- PUBLIC INTERFACE -----------------

(defun reset ()
  (loop for param in *parameters*
     do (clear-parameter (get-key param))))

(defun set-configuration-file (path)
  "Define the path of the configuration file to be used in combination with the :file-only and :file-first strategies."
  (unless (uiop:probe-file* path)
    (error 'file-not-found-error :path path))

  (when (and *config-file*
	     (not (equal path *config-file*)))
    (warn "Overriding previously defined configuration file."))

  (setf *config-file* path))

(defun get-value (parameter &optional strategy force)
  "Read the value of paramter *PARAM*. The source of the value can be defined by *STRATEGY*, which accepts following values  
**nil** Reuse the same strategy as in the previous get/set of this parameter;  
**:file-first** Load the value first from the local configuration and, if it does not exist, from the database;  
**:db-first** Load the value first from the database and, if it does not exist, from the local configuration;  
**:try-append** Load the values both from database and from local configuration and try to append them (valid types are only *list* and *string*);  
**:file-only** Load the value only from the local configuration file;  
**:db-only** Load the value only from the database."
  (let ((it (find-parameter parameter)))
    (if it
	(when (or force
		  (and (not (null strategy))
		       (not (eq strategy (strategy it)))))
	  (log:warn "Had to reload parameter '~a' because of ~:[strategy change~;(force t) ~]."
		    (get-key it) force)
	  (when strategy
	    (setf (slot-value it 'read-strategy) strategy))
	  (setf (slot-value it 'value) (load-value parameter (strategy it))))
	(progn
	  (unless strategy
	    (log:warn "No strategy for loading newly created parameter '~a' given. Using :memory-only as default." parameter))
	  (setf it (make-instance 'config-parameter
				  :key parameter
				  :strategy (or strategy :memory-only)
				  :value (load-value parameter (or strategy :memory-only))))
	  (push it *parameters*)))
    (slot-value it 'value)))


(defun (setf get-value) (value parameter &optional strategy)
  "Save the parameter *PARAMETER* with value *VALUE* into storage. The storage destination is defined by the parameter *STRATEGY* with possible values being  
**nil** Reuse the same strategy as in the previous get/set of this parameter;  
**:local-only** Save the parameter/value pair only in the local configuration file;  
**:database-only** Save the parameter/value pair only in the database."
  (let ((it (find-parameter parameter)))
    (if it
	(progn
	  (setf (slot-value it 'value) value)
	  (when strategy
	    (setf (slot-value it 'read-strategy) strategy)))
	(progn
	  (unless strategy
	    (log:warn "No strategy for saving newly created parameter '~a' given. Using :memory-only as default." parameter))
	  (setf it (make-instance 'config-parameter
				       :key parameter
				       :strategy (or strategy :memory-only)
				       :value value))
	  (push it *parameters*)))

    (save-value parameter (or strategy (strategy it)) value)))

(defun clear-parameter (parameter)
  "Remove the parameter from memory (aka unload)."
  (the boolean
       (trivial-utilities:aif (find-parameter parameter)
			      (progn
				(setf *parameters* (delete it *parameters*))
				(values t))
			      (progn
				(log:warn "Parameter ~a is not present." parameter)
				(values nil)))))

(defun delete-parameter (parameter from)
  "Delete the *parameter* *from* the indicated source. Also removes it from memory.  
**:file** - Delete the *parameter* from the configuration file;   
**:database** - Delete the *parameter* from the database;    
**:everywhere** - Delete the *parameter* from both file and database"
  (error "This function is not yet implemented. Support is very much appreciated."))

