;;;; Copyright (c) Eric Diethelm 2018 <ediethelm@yahoo.com>
;;;; This code is licensed under the MIT license.

(in-package :piggyback-parameters)

(5am:def-suite :piggyback-parameters-tests :description "Piggyback Parameters tests")
(5am:in-suite :piggyback-parameters-tests)

(defmacro with-temp-folder ((folder-var) &body body)
  `(let ((,folder-var nil))
    (unwind-protect
	 (progn
	   (setf ,folder-var (uiop:ensure-pathname (uiop:merge-pathnames* (format nil "test-~5,'0x/" (1+ (random 99999)))
									 (asdf:system-source-directory :piggyback-parameters))
						  :ensure-directories-exist t))
	   ,@body)

      ;; Cleanup by removing the temprary folder
      (when ,folder-var
	(uiop:delete-directory-tree ,folder-var :if-does-not-exist :ignore :validate t)))))

(5am:test set-configuration-file
  (5am:signals file-not-found-error (set-configuration-file (format nil "test-config-~5,'0x" (1+ (random 99999)))))

  (with-temp-folder (tmp-folder)
    (with-open-file (stream (uiop:merge-pathnames* "configuration-file" tmp-folder)
			    :if-does-not-exist :create
			    :direction :output))
    (5am:finishes (set-configuration-file (uiop:merge-pathnames* "configuration-file" tmp-folder)))
    (5am:is (equal (uiop:merge-pathnames* "configuration-file" tmp-folder) *config-file*))))

(5am:test value-from-file
  (setf *parameters* nil)
  
  (with-temp-folder (tmp-folder)
    (log:info "Using ~a as temporary configuration file." (uiop:merge-pathnames* "configuration-file" tmp-folder))
    (with-open-file (stream (uiop:merge-pathnames* "configuration-file" tmp-folder)
			    :if-does-not-exist :create
			    :if-exists :supersede
			    :direction :output))
    (set-configuration-file (uiop:merge-pathnames* "configuration-file" tmp-folder))

    (5am:is (null (get-value :test1 :file-only)))
    (5am:is (eq (setf (get-value :test1 :file-only) 1234) 1234))
    
    (5am:is (eq (get-value :test1 :file-only) 1234))
    (5am:is (eq (get-value :test1) 1234))
    (5am:is (eq (setf (get-value :test1) 2345) 2345))
    (5am:is (eq (get-value :test1) 2345))
    (5am:is (string= "<<\":TEST1\",2345>>"
		     (format nil "~{~a~}"
			     (with-open-file (stream (uiop:merge-pathnames* "configuration-file" tmp-folder))
			       (loop for line = (read-line stream nil)
				  while line
				  collect line)))))

    (5am:is (string= (setf (get-value :test2 :file-only) "abcd") "abcd"))
    (5am:is (string= (get-value :test2) "abcd"))
    (5am:is (string= "<<\":TEST2\",\"abcd\">,<\":TEST1\",2345>>"
		     (format nil "~{~a~}"
			     (with-open-file (stream (uiop:merge-pathnames* "configuration-file" tmp-folder))
			       (loop for line = (read-line stream nil)
				  while line
				  collect line)))))

    (with-open-file (stream (uiop:merge-pathnames* "configuration-file" tmp-folder)
			    :if-does-not-exist :create
			    :if-exists :supersede
			    :direction :output)
      (format stream "<<\":TEST2\",\"fghi\">,<\":TEST1\",5678>>"))
    
    (5am:is (eq (get-value :test1) 2345))
    (5am:is (eq (get-value :test1 nil t) 5678))
    (5am:is (string= (get-value :test2) "abcd"))
    (5am:is (string= (get-value :test2 nil t) "fghi"))))

(5am:test value-from-memory
  (setf *parameters* nil)
  
  (with-temp-folder (tmp-folder)
    (log:info "Using ~a as temporary configuration file." (uiop:merge-pathnames* "configuration-file" tmp-folder))
    (with-open-file (stream (uiop:merge-pathnames* "configuration-file" tmp-folder) :if-does-not-exist :create :direction :output)
      (format stream "<<\":TEST2\":\"fghi\">,<\":TEST1\":5678>>"))
    
    (set-configuration-file (uiop:merge-pathnames* "configuration-file" tmp-folder))

    (5am:is (null (get-value :test1 :memory-only)))
    (5am:is (eq (setf (get-value :test1 :memory-only) 1234) 1234))
    
    (5am:is (eq (get-value :test1 :memory-only) 1234))
    (5am:is (eq (get-value :test1) 1234))
    (5am:is (eq (setf (get-value :test1) 2345) 2345))
    (5am:is (eq (get-value :test1) 2345))
    (5am:is (string= "<<\":TEST2\":\"fghi\">,<\":TEST1\":5678>>"
		     (format nil "~{~a~}"
			     (with-open-file (stream (uiop:merge-pathnames* "configuration-file" tmp-folder))
			       (loop for line = (read-line stream nil)
				  while line
				  collect line)))))

    (5am:is (string= (setf (get-value :test2 :memory-only) "abcd") "abcd"))
    (5am:is (string= (get-value :test2) "abcd"))
    (5am:is (string= "<<\":TEST2\":\"fghi\">,<\":TEST1\":5678>>"
		     (format nil "~{~a~}"
			     (with-open-file (stream (uiop:merge-pathnames* "configuration-file" tmp-folder))
			       (loop for line = (read-line stream nil)
				  while line
				  collect line)))))))

(5am:test value-from-database
  (5am:skip "No database mock avaliable."))

(5am:test clear-parameter
  (setf *parameters* nil)

  (setf (get-value :test1 :memory-only) 1234)
  (5am:is (eq (length *parameters*) 1))
  (5am:is (not (null (find-parameter :test1))))
  (5am:is (not (null (clear-parameter :test1))))
  (5am:is (null (clear-parameter :test2)))
  (5am:is (null *parameters*))
  
  (setf (get-value :test1 :memory-only) 1234)
  (setf (get-value :test2 :memory-only) 6789)
  (5am:is (eq (length *parameters*) 2))
  (5am:is (not (null (find-parameter :test1))))
  (5am:is (not (null (clear-parameter :test1))))
  (5am:is (eq (length *parameters*) 1))
  (5am:is (not (null (find-parameter :test2)))))
  
  
  
